# Ubuntu 22.04 LTS
FROM docker.io/ubuntu:22.04

RUN apt-get update && apt-get install -y \
    bzip2 \
    curl \
    gcc \
    make \
    patch \
    tar

# Download the shim tarball, verify the file hash matches, and extract.
RUN mkdir -p /build/shim
WORKDIR /build/shim
ADD manifest .
RUN curl --location --remote-name https://github.com/rhboot/shim/releases/download/15.7/shim-15.7.tar.bz2
RUN sha256sum --check manifest
RUN tar -jxvpf shim-15.7.tar.bz2 && rm shim-15.7.tar.bz2
WORKDIR /build/shim/shim-15.7

# Apply patches
ADD patches patches
RUN patch -p1 -i patches/0001-Enable-the-NX-compatibility-flag-by-default.patch

# Add our public certificate
ADD chromeos_reven.cer .

# Add our SBAT data
ADD sbat.csv data/sbat.csv

# Create build directories
RUN mkdir build-x64 build-ia32

# Set the path of reven's certificate, and set TOPDIR since we build in
# arch-specific subdirectories.
ENV BUILD_FLAGS= \
    VENDOR_CERT_FILE=../chromeos_reven.cer \
    TOPDIR=..

# Disable EBS protection since crdyboot verifies the kernel (and kernel
# args) with ChromeOS vboot, which doesn't go through shim's
# verification hook.
ENV BUILD_FLAGS=${BUILD_FLAGS} DISABLE_EBS_PROTECTION=y

# Build 64-bit
RUN make -C build-x64 ARCH=x86_64 DEFAULT_LOADER='\\crdybootx64.efi' \
    ${BUILD_FLAGS} -f ../Makefile

# Build 32-bit
RUN make -C build-ia32 ARCH=ia32 DEFAULT_LOADER='\\crdybootia32.efi' \
    ${BUILD_FLAGS} -f ../Makefile

# Copy the shims to a convenient location
RUN mkdir /build/install
RUN cp build-x64/shimx64.efi /build/install
RUN cp build-ia32/shimia32.efi /build/install

# Print the SHA256 of the shims.
RUN sha256sum /build/install/*
