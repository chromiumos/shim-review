TAG=shim-build

CONTAINER_CMD?=podman

build:
	${CONTAINER_CMD} build -t ${TAG} .

# Build without cache to make grabbing the full build log easier
build-no-cache:
	${CONTAINER_CMD} build --no-cache -t ${TAG} .

# Run the container to copy the two shim builds to the host
copy:
	mkdir -p install
ifeq (${CONTAINER_CMD},podman)
	${CONTAINER_CMD} run --volume $(shell pwd)/install:/host ${TAG} cp -r /build/install /host
else
	${CONTAINER_CMD} run --user $(shell id -u):$(shell id -g) --volume $(shell pwd)/install:/host ${TAG} cp -r /build/install /host
endif
	mv install/install/* . && rm -r install

# Print details of the public certificate
cert-info:
	openssl x509 -inform der -in chromeos_reven.cer -noout -text

# Dump ".sbat" section of the builds
dump-sbat:
	objdump -j .sbat -s *.efi

.PHONY: build build-no-cache cert-info copy dump-sbat
