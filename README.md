*******************************************************************************
### What organization or people are asking to have this signed?
*******************************************************************************
Google

*******************************************************************************
### What product or service is this for?
*******************************************************************************
ChromeOS (reven board)

*******************************************************************************
### What's the justification that this really does need to be signed for the whole world to be able to boot it?
*******************************************************************************
ChromeOS is a Linux distribution. We want to enable (and encourage) our
user base to boot ChromeOS (reven) with secure boot enabled.

*******************************************************************************
### Why are you unable to reuse shim from another distro that is already signed?
*******************************************************************************
Reusing another distro's shim would require reusing their grub and
kernel as well. We do not use grub, and we need to build our own kernel,
so this would not work.

*******************************************************************************
### Who is the primary contact for security updates, etc.?
*******************************************************************************
- Name: Nicholas Bishop
- Position: Software Engineer
- Email address: nicholasbishop@google.com
- PGP key fingerprint: [nbishop.key](nbishop.key)

(Key should be signed by the other security contacts, pushed to a keyserver
like keyserver.ubuntu.com, and preferably have signatures that are reasonably
well known in the Linux community.)

*******************************************************************************
### Who is the secondary contact for security updates, etc.?
*******************************************************************************
- Name: Paul Nardini
- Position: Engineering Manager
- Email address: nardini@google.com
- PGP key fingerprint: [pnardini.key](pnardini.key)

(Key should be signed by the other security contacts, pushed to a keyserver
like keyserver.ubuntu.com, and preferably have signatures that are reasonably
well known in the Linux community.)

*******************************************************************************
### Were these binaries created from the 15.7 shim release tar?
Please create your shim binaries starting with the 15.7 shim release tar file: https://github.com/rhboot/shim/releases/download/15.7/shim-15.7.tar.bz2

This matches https://github.com/rhboot/shim/releases/tag/15.7 and contains the appropriate gnu-efi source.

*******************************************************************************
We confirm that our shim binaries are built from the referenced tarball.

*******************************************************************************
### URL for a repo that contains the exact code which was built to get this binary:
*******************************************************************************
https://github.com/rhboot/shim/tree/15.7

*******************************************************************************
### What patches are being applied and why:
*******************************************************************************
* [Enable the NX compatibility flag by default](https://github.com/rhboot/shim/commit/7c7642530fab73facaf3eac233cfbce29e10b0ef) -
  Required until a new shim is released
  (<https://github.com/rhboot/shim-review/issues/307>)

*******************************************************************************
### If shim is loading GRUB2 bootloader what exact implementation of Secureboot in GRUB2 do you have? (Either Upstream GRUB2 shim_lock verifier or Downstream RHEL/Fedora/Debian/Canonical-like implementation)
*******************************************************************************
N/A

*******************************************************************************
### If shim is loading GRUB2 bootloader and your previously released shim booted a version of grub affected by any of the CVEs in the July 2020 grub2 CVE list, the March 2021 grub2 CVE list, the June 7th 2022 grub2 CVE list, or the November 15th 2022 list, have fixes for all these CVEs been applied?
* CVE-2020-14372
* CVE-2020-25632
* CVE-2020-25647
* CVE-2020-27749
* CVE-2020-27779
* CVE-2021-20225
* CVE-2021-20233
* CVE-2020-10713
* CVE-2020-14308
* CVE-2020-14309
* CVE-2020-14310
* CVE-2020-14311
* CVE-2020-15705
* CVE-2021-3418 (if you are shipping the shim_lock module)

* CVE-2021-3695
* CVE-2021-3696
* CVE-2021-3697
* CVE-2022-28733
* CVE-2022-28734
* CVE-2022-28735
* CVE-2022-28736
* CVE-2022-28737

* CVE-2022-2601
* CVE-2022-3775
*******************************************************************************
N/A

*******************************************************************************
### If these fixes have been applied, have you set the global SBAT generation on your GRUB binary to 3?
*******************************************************************************
N/A

*******************************************************************************
### Were old shims hashes provided to Microsoft for verification and to be added to future DBX updates?
### Does your new chain of trust disallow booting old GRUB2 builds affected by the CVEs?
*******************************************************************************
Pre-SBAT shim builds have been sent to Microsoft for revocation. Our
current cert has not been used to sign anything pre-SBAT.

*******************************************************************************
### If your boot chain of trust includes a Linux kernel:
### Is upstream commit [1957a85b0032a81e6482ca4aab883643b8dae06e "efi: Restrict efivar_ssdt_load when the kernel is locked down"](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=1957a85b0032a81e6482ca4aab883643b8dae06e) applied?
### Is upstream commit [75b0cea7bf307f362057cc778efe89af4c615354 "ACPI: configfs: Disallow loading ACPI tables when locked down"](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=75b0cea7bf307f362057cc778efe89af4c615354) applied?
### Is upstream commit [eadb2f47a3ced5c64b23b90fd2a3463f63726066 "lockdown: also lock down previous kgdb use"](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=eadb2f47a3ced5c64b23b90fd2a3463f63726066) applied?
*******************************************************************************
Yes, all three commits are in the `chromeos-5.10` branch our kernel is built from:
https://chromium.googlesource.com/chromiumos/third_party/kernel/+/refs/heads/chromeos-5.10

*******************************************************************************
### Do you build your signed kernel with additional local patches? What do they do?
*******************************************************************************
Our kernel is built from the `chromeos-5.10` branch:
https://chromium.googlesource.com/chromiumos/third_party/kernel/+/refs/heads/chromeos-5.10

This is the same kernel branch as all other ChromeOS devices that are on
the 5.10 kernel. The `chromeos-5.10` branch frequently merges the latest
changes from the [5.10 stable
kernel](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git).
There are a significant number of backports and ChromeOS-specific
patches, too many to list here. ChromeOS-specific patches can be
identified by the `CHROMIUM:` prefix in the commit message.

*******************************************************************************
### If you use vendor_db functionality of providing multiple certificates and/or hashes please briefly describe your certificate setup.
### If there are allow-listed hashes please provide exact binaries for which hashes are created via file sharing service, available in public with anonymous access for verification.
*******************************************************************************
We do not use this functionality.

*******************************************************************************
### If you are re-using a previously used (CA) certificate, you will need to add the hashes of the previous GRUB2 binaries exposed to the CVEs to vendor_dbx in shim in order to prevent GRUB2 from being able to chainload those older GRUB2 binaries. If you are changing to a new (CA) certificate, this does not apply.
### Please describe your strategy.
*******************************************************************************
N/A: we already switched to a new certificate for our Shim 15.4 submission:
https://github.com/rhboot/shim-review/issues/204

*******************************************************************************
### What OS and toolchain must we use to reproduce this build?  Include where to find it, etc.  We're going to try to reproduce your build as closely as possible to verify that it's really a build of the source tree you tell us it is, so these need to be fairly thorough. At the very least include the specific versions of gcc, binutils, and gnu-efi which were used, and where to find those binaries.
### If the shim binaries can't be reproduced using the provided Dockerfile, please explain why that's the case and what the differences would be.
*******************************************************************************
The [`Dockerfile`](Dockerfile) in this repository will reproduce our shim build.
As a convenience, `make build-no-cache` will do a clean build.

*******************************************************************************
### Which files in this repo are the logs for your build?
This should include logs for creating the buildroots, applying patches, doing the build, creating the archives, etc.
*******************************************************************************
[`build_log.txt`](build_log.txt)

*******************************************************************************
### What changes were made since your SHIM was last signed?
*******************************************************************************
* Added the required NX-compat patch.
* Changed second-stage bootloader from grub to [crdyboot], a UEFI
  bootloader that uses [vboot] for loading and verifying the
  kernel. This allows ChromeOS Flex to boot more like regular
  Chromebooks, and also ensures that the kernel command line is
  validated in addition to the kernel data (not unlike [UKI]).

[UKI]: https://github.com/uapi-group/specifications/blob/main/specs/unified_kernel_image.md
[crdyboot]: https://chromium.googlesource.com/chromiumos/platform/crdyboot/+/HEAD#crdyboot
[vboot]: https://chromium.googlesource.com/chromiumos/platform/vboot_reference/+/HEAD

*******************************************************************************
### What is the SHA256 hash of your final SHIM binary?
*******************************************************************************
```
fe3be90c56aa829949ad716126eaab3dfeb610fc216209cd6ab0ae0eac5eccb3  shimia32.efi
9983bd0887430bf6f86b99bb0595eeb2cfa2da09c2baef7803880092c45bde66  shimx64.efi
```

*******************************************************************************
### How do you manage and protect the keys used in your SHIM?
*******************************************************************************
The keys used in this shim are generated and stored in an HSM. They are
then encrypted for export to a signing fleet for usage in build signing
by our CI pipeline, where they remain encrypted at rest. Only 4 trusted
individuals in the org have access to the signing fleet machines,
enforced by ACL and 2FA.

*******************************************************************************
### Do you use EV certificates as embedded certificates in the SHIM?
*******************************************************************************
No.

*******************************************************************************
### Do you add a vendor-specific SBAT entry to the SBAT section in each binary that supports SBAT metadata ( grub2, fwupd, fwupdate, shim + all child shim binaries )?
### Please provide exact SBAT entries for all SBAT binaries you are booting or planning to boot directly through shim.
### Where your code is only slightly modified from an upstream vendor's, please also preserve their SBAT entries to simplify revocation.
*******************************************************************************

[shim SBAT](sbat.csv):
```
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
shim,3,UEFI shim,shim,1,https://github.com/rhboot/shim
shim.chromeos,2,ChromeOS,shim,15.7,https://chromium.googlesource.com/chromiumos/shim-review
```

[crdyboot SBAT](https://chromium.googlesource.com/chromiumos/platform/crdyboot/+/HEAD/crdyboot/sbat.csv):
```
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
crdyboot,1,Google,crdyboot,1.0.0,https://chromium.googlesource.com/crdyboot
```

*******************************************************************************
### Which modules are built into your signed grub image?
*******************************************************************************
N/A

*******************************************************************************
### What is the origin and full version number of your bootloader (GRUB or other)?
*******************************************************************************
Crdyboot 1.0.0

* [Repo](https://chromium.googlesource.com/chromiumos/platform/crdyboot/+/HEAD)
* [Build script](https://chromium.googlesource.com/chromiumos/overlays/chromiumos-overlay/+/HEAD/sys-boot/crdyboot/crdyboot-9999.ebuild)

*******************************************************************************
### If your SHIM launches any other components, please provide further details on what is launched.
*******************************************************************************
N/A

*******************************************************************************
### If your GRUB2 launches any other binaries that are not the Linux kernel in SecureBoot mode, please provide further details on what is launched and how it enforces Secureboot lockdown.
*******************************************************************************
N/A

*******************************************************************************
### How do the launched components prevent execution of unauthenticated code?
*******************************************************************************
Our shim will only launch our signed crdyboot build, which in turn will
only launch our signed kernel. The kernel is configured to enable lockdown.

See crdyboot's [boot flow] doc for more details.

[boot flow]: https://chromium.googlesource.com/chromiumos/platform/crdyboot/+/refs/heads/main/docs/boot_flow.md

*******************************************************************************
### Does your SHIM load any loaders that support loading unsigned kernels (e.g. GRUB)?
*******************************************************************************
No

*******************************************************************************
### What kernel are you using? Which patches does it includes to enforce Secure Boot?
*******************************************************************************
Our kernel is based on 5.10 and enforces lockdown.
Source repo: https://chromium.googlesource.com/chromiumos/third_party/kernel/+/refs/heads/chromeos-5.10

*******************************************************************************
### Add any additional information you think we may need to validate this shim.
*******************************************************************************
N/A
